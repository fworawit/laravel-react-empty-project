# ตั้งค่า environment
```
Copy .env.example to .env
```

เริ่มต้น start docker
```
docker compose up -d
```

# ติดตั้ง Laravel
เข้าไปที่เครื่องเสมือน
```
docker exec -it laravel-react-api bash
```

ติดตั้ง laravel
```
rm -rf supervisord.pid
composer create-project laravel/laravel .
```
![Alt text](image.png)

ตั้งค่า Permission การเข้าถึงไฟล์
```
chmod 777 -R .
```
* ไม่ควรทำบน production

ทดสอบเข้าระบบที่ `http://localhost:7106/`
![Alt text](image-1.png)

# ติดตั้ง React
```
docker exec -it laravel-react-app bash
```
สร้าง vite
```
npm create vite
cd app
cp -avr * ../
cd /app
rm -rf app
```
ตั้งค่า Permission การเข้าถึงไฟล์
```
chmod 777 -R .
```
ติดตั้ง Package
```
npm install
```
Set port
![Alt text](image-2.png)

```
npm run dev
```
การออกจาก development mode
```
Ctrl + c
```
ติดตั้ง Package สำหรับการพัฒนา
```
npm install react-router-dom 
npm install react-bootstrap bootstrap
npm install sass
npm install @ckeditor/ckeditor5-react @ckeditor/ckeditor5-build-classic

```
Document Bootstrap
```
https://react-bootstrap.github.io/docs/
```
# ปัญหาที่อาจเกิดขึ้น
### Error 429 Laravel
If you open an app/Http/Kernel.php file and find a $middlewareGroups variable, you will see throttle:api middleware:

```
How to Fix
```
วิธีแก้เวลาสั่ง
docker exec -it lravel-react-api bash แล้ว error ว่า
Error response form demon : Container 
สาเหตุเนื่องจากใช้คำสั่งนี้ไม่ได้
exec /docker-entrypoint.sh: no such file or directory
ดังนั้นให้แก้ดังนี้

git config --global core.autocrlf input
git rm --cached -r .
git reset --hard

แล้วให้
docker-compose up -d --build ใหม่
***ไม่ต้องแก้ที่บรรทัด 127****

สาเหตุ
ใน unix ขึ้นบรรทัดใหม่คือ \n แต่ windows ฉลาด(เกิน) เห็นขึ้นบรรทัดใหม่ก็เติม \r ให้ด้วยดังนั้นขึ้นบรรทัดใหม่เลยกลายเป็น \r\n แล้ว docker ก็เลยหาการขึ้นบรรทัดไม่เจอ

